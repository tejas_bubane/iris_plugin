module IrisCore
  module Model
    class Participant < ActiveRecord::Base; end
    class Account < ActiveRecord::Base; end
  end
end

require 'iris_core/model/participant.rb'
require 'iris_core/model/account.rb'
