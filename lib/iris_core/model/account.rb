module IrisCore
  module Model
    class Account < ActiveRecord::Base
      belongs_to :participant, class: IrisCore::Model::Participant
    end
  end
end
