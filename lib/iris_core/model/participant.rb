module IrisCore
  module Model
    class Participant < ActiveRecord::Base
      extend Forwardable
      def_delegator :account, :points_balance, :balance
      def_delegator :account, :points_earned, :earned
      def_delegator :account, :points_spent, :spent
      has_one :account, class: IrisCore::Model::Account

      def scheme_name
        'instance scheme name'
      end

      def self.scheme_name
        'class scheme name'
      end
    end
  end
end
