$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "iris_core/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "iris_core"
  s.version     = IrisCore::VERSION
  s.authors     = ["Tejas Bubane"]
  s.email       = ["tejas.bubane@infibeam.net"]
  s.homepage    = "TODO"
  s.summary     = "TODO: Summary of IrisCore."
  s.description = "TODO: Description of IrisCore."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.0"

  s.add_development_dependency "mysql2"
end
